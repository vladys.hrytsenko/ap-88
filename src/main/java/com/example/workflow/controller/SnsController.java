package com.example.workflow.controller;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SnsController {

    @Autowired
    private AmazonSNSClient amazonSNSClient;

    private String TOPIC_ARN = "arn:aws:sns:us-east-1:563229348140:my-sns-topic";

    @PostMapping("/createTopic")
    private String createTopic(@RequestParam("topicName") String topicName) {
        CreateTopicRequest topicCreateRequest = new CreateTopicRequest(topicName);
        CreateTopicResult topicCreateResponse = amazonSNSClient.createTopic(topicCreateRequest);

        System.out.println("Topic creation successful");
        return topicCreateResponse.getTopicArn();
    }

    @GetMapping("/subscribe/{email}")
    public String subscribeToSNSTopic(@PathVariable String email) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(TOPIC_ARN,"email", email);

        amazonSNSClient.subscribe(subscribeRequest);
        return "To confirm the subscription, check your email.";
    }

    @GetMapping("/publish/{msg}")
    public String publishToTopic(@PathVariable String msg){
        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, msg,"SNS SpringBoot");

        amazonSNSClient.publish(publishRequest);
        return "Message publishing successful!";
    }
}
